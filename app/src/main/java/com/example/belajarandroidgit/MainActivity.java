package com.example.belajarandroidgit;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String ID_valid = "Khuro11";
        String PASSWORD_valid = "makinoye";
        EditText edID = (EditText) findViewById(R.id.ETUSERNAME);
        EditText edPASSWORD = (EditText) findViewById(R.id.ETPassword);
        Button btnSUBMIT = (Button) findViewById(R.id.btnSUBMIT);
        btnSUBMIT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((edID.getText().toString().trim().isEmpty()) ||
                        (edPASSWORD.getText().toString().trim().isEmpty())) {
                    // JIKA KEDUA ATAU SALAH SATU ISIAN ADA YANG KOSONG
                    Toast.makeText(getApplicationContext(), "Isian Tidak Valid", Toast.LENGTH_LONG).show();
                } else {
                    // JIKA KEDUA ISIAN TELAH DI TERISI
                    if ((edID.getText().toString().trim().equals(ID_valid))
                            && (edPASSWORD.getText().toString().trim().equals(PASSWORD_valid))) {
                        // Toast.makeText(getApplicationContext(), "Login Berhasil", Toast.LENGTH_LONG).show();
                        Intent intent_Dashboard = new
                                Intent(MainActivity.this, DasboarAPK.class);
                        intent_Dashboard.putExtra("", ID_valid);
                        startActivity(intent_Dashboard);
                    } else {
                        Toast.makeText(getApplicationContext(), "Login Gagal", Toast.LENGTH_LONG).show();
                                //test
                    }
                }
            }
        });
    }
}
